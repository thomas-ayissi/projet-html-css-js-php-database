<?php
class User
{
    //PRIVATE
    private $id;
    private $email;
    private $name;
    private $lastname;
    private $town;
    private $postal;
    private $address;
    private $active;

    //PUBLIC
    public function __construct($email, $name, $lastname, $town, $postal, $address, $active = 1)
    {
        $this->email = $email;
        $this->name = $name;
        $this->lastname = $lastname;
        $this->town = $town;
        $this->postal = $postal;
        $this->address = $address;
        $this->active = $active;
    }

    //SET
    public function setID($id)
    {
        $this->id = $id;
    }


    //GET
    public function getID(){
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function getTown(){
        return $this->town;
    }

    public function getZip(){
        return $this->postal;
    }

    public function getAddress(){
        return $this->address;
    }

    //FONCTIONS
    public function toArray()
    {
        return array(
            "id" => $this->id,
            "email" => $this->email,
            "name" => $this->name,
            "lastname" => $this->lastname,
            "town" => $this->town,
            "postal" => $this->postal,
            "address" => $this->address,
            "active" => $this->active
        );
    }
}
