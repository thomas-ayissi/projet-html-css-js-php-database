<?php
class Form{
//PRIVATE
    private $title;
    private $fieldset;
    private $inputs;
    private $others;
    private $reset;
    private $submit;

    private $action;
    private $target;
    private $method;

    private function isdisabled($disabled){
        if($disabled){
            return "disabled";
        }
        return null;
    }

//PUBLIC
    public function __construct($title=NULL,$action=NULL,$target=NULL,$method=NULL) {
        $this->title = $title;
        $this->fieldset = false;
        $this->inputs = array();
        $this->reset = array(
            'id' => null,
            'type' => 'reset',
            'class' => array(),
            'disabled' => false,
            'content' => null
        );
        $this->submit = array(
            'id' => null,
            'type' => 'submit',
            'class' => array(),
            'disabled' => false,
            'content' => null
        );

        $this->action = $action;
        $this->target = $target;
        $this->method = $method;
    }

    //SET

    /**
     * @param bool $fieldset
     */
    public function setFieldset($fieldset){
        if(is_bool($fieldset)){
            $this->fieldset = $fieldset;
        }
    }

    /**
     * @param string $action
     */
    public function setAction($action){
        if(is_string($action)){
            $this->action = $action;
        }
    }

    /**
     * @param string $target
     */
    public function setTarget($target){
        if(is_string($target)){
            $this->target = $target;
        }
    }

    /**
     * @param string $method
     */
    public function setMethod($method){
        if(strtolower($method) == "get" || strtolower($method) == "post"){
            $this->method = $method;
        }
    }

    public function setSubmit($id, $disabled, $content){
        $this->submit['id'] = $id;
        $this->submit['disabled'] = $disabled;
        $this->submit['content'] = $content;
    }

    public function setReset($id, $disabled, $content){
        $this->reset['id'] = $id;
        $this->reset['disabled'] = $disabled;
        $this->reset['content'] = $content;
    }

    //FONCTIONS

    public function addClassReset($class){
        array_push($this->reset['class'],$class);
    }

    public function addClassSubmit($class){
        array_push($this->submit['class'],$class);
    }

    public function stringClass($listClass){
        $string = "";
        foreach ($listClass as $class){
            $string .= $class." ";
        }
        return $string;
    }

    /**
     * @param Input $input
     */
    public function addInput($input){
        if(is_a($input,'Input')){
            array_push($this->inputs, $input);
        }
    }


    public function containInputFile(){
        foreach($this->inputs as $input){
            if($input->getType() == "file"){
                return true;
            }
        }
        return false;
    }

    public function htmlInputs(){
        $string = "";
        foreach ($this->inputs as $input){
            $string .= $input->toHTML();
        }
        return $string;
    }

    /**
     * @param struct reset or submit 
     */
    public function htmlResetOrSubmit($struct){
        if(!empty($struct['content'])){
            return '<button id="'.$struct['id'].'" 
            class="'.$this->stringClass($struct['class']).'" 
            type="'.$struct['type'].'" '.
            $this->isdisabled($struct['disabled'])
            .'>'.$struct['content'].'</button>';
        }
    }

    public function toHTML(){
        $HTML = '<form';
        if(!empty($this->action)){
            $HTML .= ' action="'.$this->action.'"';
        }
        if(!empty($this->target)){
            $HTML .= ' target="'.$this->target.'"';
        }
        if(!empty($this->method)){
            $HTML .= ' method="'.$this->method.'"';
        }
        if($this->containInputFile()){
            $HTML .= ' enctype="multipart/form-data"';
        }
        $HTML .= '>';

        if($this->fieldset){
            $HTML .= '<fieldset>';

            if (!empty($this->title)){
                $HTML .= '<legend>'.$this->title.'</legend>';
            }
        }
        else if (!empty($this->title)){
            $HTML .= '<h2>'.$this->title.'</h2>';
        }


        $HTML .= $this->htmlInputs();

        $HTML .= $this->htmlResetOrSubmit($this->reset);

        $HTML .= $this->htmlResetOrSubmit($this->submit);


        if($this->fieldset){
            $HTML .= '</fieldset>';
        }

        $HTML .= "</form>";
        return $HTML;
    }
}