<?php
class Input
{
    //PRIVATE
    private $type;
    private $id;
    private $name;
    private $placeholder;
    private $pattern;
    private $required;
    private $autofocus;

    private $value;
    private $maxlength;

    private $help;

    private $href;
    private $content;

    private $label;
    private $displayOption;

    private $min;
    private $max;

    private $subList;

    private $function;

    private $class;

    private $div;
    private $p;

    //PUBLIC

    public function __construct($type = "text", $id = NULL, $name = NULL, $config = NULL)
    {
        $this->type = $type;
        $this->id = $id;
        $this->name = $name;

        //CONFIG
        $this->placeholder = $config['placeholder'] ?? NULL;

        $this->required = $config['required'] ?? false;
        $this->autofocus = $config['autofocus'] ?? false;

        $this->help = $config['help'] ?? false;

        $this->value = $config['value'] ?? NULL;
        $this->content = $config['content'] ?? NULL;

        $this->label = $config['label'] ?? NULL;
        $this->displayOption = $config['displayOption'] ?? false;

        if ($this->type == "range" || $this->type == "date") {
            $this->min = $config['min'] ?? NULL;
            $this->max = $config['max'] ?? NULL;
        }

        if ($this->type == "select" || $this->type == "radio" || $this->type == "listCheckBox") {
            $this->subList = $config['subList'] ?? NULL;
        }

        $this->function = $config['function'] ?? NULL;
        $this->class = $config['class'] ?? NULL;
        $this->pattern = $config['pattern'] ?? NULL;
        $this->div = $config['div'] ?? false;
        $this->p = $config['p'] ?? NULL;
        $this->maxlength = $config['maxlength'] ?? NULL;
    }

    //SET
    //TOUS LES SETTER ONT ÉTÉ RETIRÉS...

    //GET
    public function getID()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getAutofocus()
    {
        if ($this->autofocus) {
            return "autofocus";
        }
        return "";
    }

    public function getRequired()
    {
        if ($this->required) {
            return "required";
        }
        return "";
    }

    //FONCTIONS

    public function autovalue()
    {
        if (!empty($this->value)) {
            return 'value="' . $this->value . '"';
        }
        return null;
    }

    public function isOptional()
    {
        if (!$this->required) {
            return 'optional';
        }
    }

    public function addClass($class)
    {
        array_push($this->class, $class);
    }

    public function htmlClass()
    {
        if (!empty($this->class)) {
            $string = "";
            foreach ($this->class as $class) {
                $string .= $class . " ";
            }
            return $string;
        }
    }

    public function htmlLabel()
    {
        return '<label class="' . $this->isOptional() . '" id="label-' . $this->name . '" for="' . $this->id . '">' . $this->label . '</label>';
    }

    public function htmlMinMax()
    {
        if ((!empty($this->min) && !empty($this->max)) &&
            ($this->type == 'range' || $this->type == 'date')
        ) {
            return 'min="' . $this->min . '" max="' . $this->max . '"';
        }
    }

    public function htmlFunction()
    {
        if (!empty($this->function)) {
            return $this->function;
        }
    }

    public function htmlreturnImage()
    {
        if ($this->type == 'file') {
            return '<div id="Pic_div">
                        <img id="display" src="#" alt="votre image">
                    </div>';
        }
    }


    public function htmlPlaceholder()
    {
        if (!empty($this->placeholder)) {
            return 'placeholder="' . $this->placeholder . '"';
        }
    }

    public function htmlPattern()
    {
        if (!empty($this->pattern)) {
            return 'pattern="' . $this->pattern . '"';
        }
    }

    public function htmlP()
    {
        if (!empty($this->p)) {
            $string = "";
            foreach ($this->p as $val) {
                $string .= '<p>' . $val . '</p>';
            }
            return $string;
        }
    }

    public function htmlMaxLenght()
    {
        if (!empty($this->maxlength)) {
            return 'maxlenght="'.$this->maxlength.'"';
        }
    }

    public function htmlHelp()
    {
        if ($this->help) {
            return '<div id="label_help">
                        ' . $this->htmlLabel() . '
                        <div class="tooltip">&#10068;
                            <span class="tooltiptext">- minimum 8 caracteres <br>- au moins une majuscule <br>- au moins un chiffre</span>
                        </div>
                    </div>';
        }
        return $this->htmlLabel();
    }

    public function htmlRadio(){
        if(!empty($this->subList)){
            $string = "";
            foreach ($this->subList as $radio){
                $string .= '<div>
                                <label class="container-radio" for="'.$radio['value'].'">'.$radio['content'].'
                                    <input type="radio" id="'.$radio['value'].'" name="'.$this->getName().'" value="'.$radio['value'].'">
                                    <span class="checkmark-radio"></span>
                                </label>
                            </div>';
            }
            return $string;
        }
    }

    public function htmlCheckBox(){
        if(!empty($this->subList)){
            $string = "";
            foreach ($this->subList as $checkbox){
                $string .= '<div>
                                <label class="container" for="'.$checkbox['id'].'">'.$checkbox['content'].'
                                    <input type="checkbox" name="'.$this->getName().'[]" id="'.$checkbox['id'].'" value="'.$checkbox['value'].'">
                                    <span class="checkmark"></span>
                                </label>
                            </div>';
            }
            return $string;
        }
    }

    public function htmlOptions()
    {
        $string = "";
        foreach ($this->subList as $option) {
            $id = $option['id'] ?? NULL;
            $string .= '<option id="' . $id . '" value="' . $option['value'] . '">' . $option['content'] . '</option>';
        }
        return $string;
    }

    /**
     * displayOption = 1 --> standard Layout
     * else label before input
     */
    public function toHTML()
    {
        if ($this->displayOption) {
            return '
                    <div id="' . strtoupper(substr($this->name, 0, 2)) . '" class="' . $this->htmlClass() . '">
                        <input type="' . $this->type . '" id="' . $this->id . '" name="' . $this->name . '" '
                . $this->htmlFunction() . ' ' . $this->htmlPlaceholder() . ' ' . $this->htmlPattern() . ' '
                . $this->htmlMinMax() . ' ' . $this->autovalue() . ' ' . $this->getAutofocus() . ' '
                . $this->getRequired() . '>'
                . $this->htmlLabel() . ' ' . $this->htmlreturnImage()
                . '</div>
                ';
        }
        switch ($this->type) {
            case 'link':
                return '<a id="' . $this->id . '" class="' . $this->htmlClass() . '" href="' . $this->href . '">' . $this->content . '</a>';

            case 'password':
                return '<div id="' . strtoupper(substr($this->name, 0, 2)) . '" class="' . $this->htmlClass() . '">' .
                    $this->htmlHelp()
                    . '<input type="password" id="' . $this->id . '" name="' . $this->name . '" ' . $this->htmlPattern() . ' ' . $this->htmlPlaceholder() . ' ' . $this->getRequired() . '>
                        </div>';

            case 'checkbox':
                if ($this->div) {
                    return '<div class="formCheck">
                                            <label class="container"> ' . $this->getLabel() . '
                                                <input type="' . $this->getType() . '" name="' . $this->getName() . '" id="' . $this->getID() . '" ' . $this->getRequired() . '>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>';
                }
                return '
                                        <label class="container"> ' . $this->getLabel() . '
                                            <input type="' . $this->getType() . '" name="' . $this->getName() . '" id="' . $this->getID() . '" ' . $this->getRequired() . '>
                                            <span class="checkmark"></span>
                                        </label>
                                    ';

            case 'textarea':
                return '
                    <div id="' . strtoupper(substr($this->getName(), 0, 4)) . '" class="formRow">
                        ' . $this->htmlLabel() . '
                        <textarea name="' . $this->getName() . '" id="' . $this->getID() . '" ' . $this->htmlPlaceholder() . ' ' . $this->getRequired() . ' '.$this->htmlMaxLenght().'></textarea>
                        ' . $this->htmlP() . '
                    </div>
                ';

            case 'select':
                return '
                    <div id="' . strtoupper(substr($this->getName(), 0, 3)) . '" class="' . $this->htmlClass() . '">
                        ' . $this->htmlLabel() . '
                        <select id="' . $this->getID() . '" name="' . $this->getName() . '">
                        ' . $this->htmlOptions() . '
                        </select>
                    </div>
                ';

            case 'radio':
                return '
                    <div id="' . strtoupper(substr($this->getName(), 0, 2)) . '" class="' . $this->htmlClass() . '">'
                        . $this->htmlLabel() . '
                        <div>
                        ' . $this->htmlRadio() . '
                        </div>
                    </div>
                ';

            case 'listCheckBox':
                return '
                    <div id="'.strtoupper(substr($this->getName(),0,2)).'" class="'.$this->htmlClass().'">
                        '.$this->htmlLabel().'
                        <div>
                            '.$this->htmlCheckBox().'
                        </div>
                    </div>
                ';

            default:
                return '<div id="' . strtoupper(substr($this->name, 0, 2)) . '" class="' . $this->htmlClass() . '">'
                    . $this->htmlLabel()
                    . '<input type="' . $this->type . '" id="' . $this->id . '" name="' . $this->name . '" '
                    . $this->htmlFunction() . ' ' . $this->htmlPlaceholder() . ' ' . $this->htmlPattern() . ' '
                    . $this->htmlMinMax() . ' ' . $this->autovalue() . ' ' . $this->getAutofocus() . ' '
                    . $this->getRequired() . '>
                            ' . $this->htmlreturnImage() . '
                        </div>';
        }
    }
}
