<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Présentation";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <div id="MainAside">
        <main id="Presentation">
            <div class="side"></div>
            <div id="content">
                <h2>Présentation</h2>
                <section>
                    <h3>Qui somme-nous ?</h3>
                    <p>Curabitur viverra nunc ac fermentum ullamcorper. Nulla id ex a ipsum vulputate fermentum. Etiam a nisl id quam vulputate vestibulum. <strong>Suspendisse semper orci sapien</strong>, sit amet ultrices urna pharetra sit amet. Aliquam quis faucibus nisl, vitae blandit orci. Maecenas erat mi, varius eget facilisis nec, vestibulum eu sem. Morbi maximus aliquet sapien, quis posuere odio rutrum id. Suspendisse est neque, condimentum nec convallis in, feugiat vel diam. Curabitur feugiat pharetra elit, sit amet eleifend ipsum scelerisque sit amet. Cras in <strong>quam eu purus tincidunt</strong> gravida et quis massa. Ut sagittis suscipit tincidunt. Praesent scelerisque dolor purus, sed vestibulum neque lobortis nec. Donec suscipit odio a tortor maximus, quis luctus velit auctor. Integer ac felis feugiat, bibendum ligula sollicitudin, sagittis est. Mauris placerat, arcu vitae iaculis tempus, odio mi volutpat nibh, sit amet efficitur velit elit ac ipsum. Suspendisse consequat euismod leo, vel viverra ante condimentum ac. Donec commodo euismod arcu, ac imperdiet tellus pulvinar vitae. Cras et elementum tortor. <strong>Morbi faucibus arcu in ligula commodo</strong>, ut suscipit magna convallis. In id eleifend orci. Cras eget sapien dui. Etiam cursus mattis sodales. Proin pharetra eget nunc ut aliquet. Aliquam erat volutpat. Phasellus gravida, turpis nec vulputate bibendum, lorem orci consectetur velit, sed dignissim libero dui ut quam.</p>
                    <img src="img/building.jpg" alt="building">
                    <p>Nulla commodo mi eu nunc viverra aliquam. Vivamus eget tristique purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Ut maximus posuere diam ut luctus. Duis posuere volutpat sem rhoncus laoreet. Maecenas sed dolor nunc.</p>
                    <p>Ut tristique egestas nulla, non maximus metus tempus eget. Aliquam erat volutpat. Donec at lacus sagittis, sodales nulla sed, accumsan neque. Donec non velit magna. Ut ultrices malesuada ante non ullamcorper. Nullam tellus magna, dapibus eget tortor non, molestie pretium enim. Sed ultrices sed erat ac molestie.</p>
                    <p>Etiam bibendum massa ipsum, ac malesuada erat eleifend quis. Sed at leo rhoncus, placerat quam vel, egestas metus. Donec quis tortor turpis. In sem dui, ultrices at porta eget, aliquam vitae lorem. Mauris eu risus nec nulla porttitor pharetra vel id augue. Phasellus turpis ex, suscipit ut laoreet in, vulputate in dolor. Fusce ultrices tempus dui, vel maximus sapien maximus et.</p>
                    <p>Sed id felis eget nibh efficitur consequat. Suspendisse condimentum blandit mauris vel tincidunt. Sed sed mollis odio, vel tempus enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum semper urna et est fermentum, a venenatis mauris interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non porttitor ipsum, et aliquam velit. Nunc eu molestie nulla. Mauris blandit aliquet orci, vel imperdiet dolor. Quisque malesuada pretium elit ut scelerisque.</p>
                </section>
                <section>
                    <h3>Notre équipe</h3>
                    <ul>
                        <li>
                            <div><img src="img/user.png" alt="membre du staff"></div>
                            <span>staff_member</span>
                        </li>
                        <li>
                            <div><img src="img/user.png" alt="membre du staff"></div>
                            <span>staff_member</span>
                        </li>
                        <li>
                            <div><img src="img/user.png" alt="membre du staff"></div>
                            <span>staff_member</span>
                        </li>
                    </ul>
                </section>
            </div>
            <div class="side"></div>
        </main>

        <aside id="Aside_Context">
            <span>&#10133;</span>
            <section>
                <h2>Nos Coordonnées</h2>
                <ul>
                    <li>
                        <a href="#"><strong>Numéro de téléphone :</strong> <?= TEL ?></a>
                    </li>
                    <li>
                        <a href="#"><strong>Adresse mail : </strong> <?= MAIL ?></a>
                    </li>
                    <li>
                        <a href="#"><strong>Adresse : </strong> <?= POST_ADDRESS ?></a>
                    </li>
                </ul>
                <h2>Nos Horaires</h2>
                <div>
                    <p>Du <strong>lundi</strong> au <strong>vendredi</strong> de 9h à 17h</p>
                    <p>Les <strong>Week-ends</strong> de 8h30 à 12h30</p>
                </div>
                <p>Pour plus d'info et d'avantage...</p>
                <a href="/inscription.php">INSCRIVEZ-VOUS !</a>
            </section>
        </aside>
    </div>

    <?php include 'modules/footer.php'; ?>
    <script src="js/js.js"></script>
    <script>
        const tab_aside = document.querySelectorAll('aside li > a');

        function clipboardValue(n) {
            clip = tab_aside[n].textContent.split(':')
            navigator.clipboard.writeText(clip[1]);
            alert("Valeur copier dans le presse-papier");
        }

        for (let i = 0; i < tab_aside.length; i++) {
            tab_aside[i].addEventListener('click', clipboardValue.bind(null, i));
        }
    </script>
</body>

</html>