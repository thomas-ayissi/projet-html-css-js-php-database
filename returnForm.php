<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");

function accountAlreadyExists($pdo)
{
    $sth = $pdo->prepare("SELECT email FROM users WHERE email = " . $pdo->quote($_POST['email']));
    $sth->execute();
    if (!empty($sth->fetch())) {
        return true;
    }
    return false;
}

function articleAlreadyExists($pdo){
    $sth = $pdo->prepare("SELECT title FROM articles WHERE title = " . $pdo->quote($_POST['title']));
    $sth->execute();
    if (!empty($sth->fetch())) {
        return true;
    }
    return false;
}

function redirect($file){       // décommenter pour voir la page returnForm.php
    //if (empty($_SESSION) || !isset($_SESSION['Admin'])){
        header('Location: '.$file);
        exit();
    //}
}



if (!empty($_GET)) {  //LOGIN
    
    include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/Syst_login.php");

    redirect('index.php');
    $next_page = 'index.php';

    $data = $_GET;

} else if (!empty($_POST)) {

    if (!isset($_POST['phone']) && !isset($_POST['author'])) {  //INSCRIPTION


        include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/Syst_signup.php");

        redirect('index.php');
        $next_page = 'index.php';

    } else if(isset($_POST['phone'])){   //CONTACT
        
        include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/Syst_sendMessage.php");

        $_SESSION['message-ok'] = true;
        redirect('contact.php');
        $next_page = 'contact.php';

    } else{        //CREATE ARTICLE
        
        $currentPath = __DIR__;
        include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/Syst_createArticle.php");

        redirect('blog.php');
        $next_page = 'blog.php';
    }

    $data = $_POST;
}
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Réponse au formulaire";
include $_SERVER["DOCUMENT_ROOT"] . '/modules/head.php';
?>

<body>
    <?php include $_SERVER["DOCUMENT_ROOT"] . '/modules/header.php'; ?>

    <main id="ReturnForm">
        <div class="side"></div>
        <div id="content">
            <p>Si vous voyez cette page, c'est que vous êtes Administrateur</p>
            <table class="RForm_table">
                <thead>
                    <?php
                    foreach ($data as $key => $value) {
                        echo "<tr>";
                        echo "<th id=\"column_" . $key . "\">" . $key . "</th>";
                        echo "</tr>";
                    }
                    ?>
                </thead>
                <tbody>
                    <?php
                    foreach ($data as $key => $value) {
                        echo "<tr>";
                        if (is_array($value)) {
                            echo "<td>";
                            foreach ($data[$key] as $k => $subvalue) {
                                echo " $subvalue";
                            }
                            echo "</td>";
                        } else {
                            echo "<td>" . $value . "</td>";
                        }

                        echo "</tr>";
                    }

                    ?>
                </tbody>
            </table>
            <?php
            if (!empty($_FILES) && !empty($_FILES['Image']["name"])) {
                $type = explode('/', $_FILES["Image"]['type'])[1];
                move_uploaded_file($_FILES["Image"]['tmp_name'], __DIR__ . "/temp/photo-uploaded." . $type);

                echo "<h2>Image</h2>";
                echo "<table class=\"RForm_table\">";
                echo "\t<thead>";
                foreach ($_FILES["Image"] as $key => $value) {
                    echo "\t\t<tr>";
                    echo "\t\t\t<th id=\"column_" . $key . "\">" . $key . "</th>";
                    echo "\t\t</tr>";
                }
                echo "\t</thead>";
                echo "\t<tbody>";
                foreach ($_FILES["Image"] as $key => $value) {
                    echo "\t\t<tr>";
                    echo "\t\t\t<td>" . $value . "</td>";
                    echo "\t\t</tr>";
                }
                echo "\t</tbody>";
                echo "</table>";
                echo "<img id=\"img_pp\" src=\"/temp/photo-uploaded." . $type . "\" alt=\"votre image\">";
            }
            echo '<a id="next_button" href="/'.$next_page.'">Suite &raquo;</a>';
            ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="js/js.js"></script>

</body>

</html>