<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Inscription";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Inscription">
        <div class="side"></div>
        <div id="content">
            <p id="introduction">Inscrivez-vous via cette page pour recevoir de multiple avantage, être notifié quand de nouvelles news sont publiées et plus encore. <br>Vous pouvez également <a href="#N-contact">aller au bas de la page</a> pour récupérer nos coordonnées et nos horaires si vous préférer nous contacter directement.</p>
            <?php
            
            $form = new Form("Inscription","/returnForm.php", null,"post");
            $form->setFieldset(true);

            $form->setReset(null, false, "Effacer");
            $form->addClassReset("Contact_button");

            $form->setSubmit("submit_button", true, "Envoyer");
            $form->addClassSubmit("Contact_button");


            $form->addInput(new Input("email", "email", "email", [
                'label' => "Adresse Email",
                'class' => ["formRow"],
                'placeholder' => "Votre adresse mail...",
                'required' => true,
                'autofocus' => true
            ]));

            $form->addInput(new Input("password", "password", "password", [
                'label' => "Mot de passe",
                'class' => ["formRow"],
                'placeholder' => "Votre mot de passe...",
                'required' => true
            ]));

            $form->addInput(new Input("password", "Cpassword", "Cpassword", [
                'label' => "Confirmation mot de passe",
                'class' => ["formRow"],
                'placeholder' => "Confirmer votre mot de passe...",
                'required' => true
            ]));

            $form->addInput(new Input("text", "fname", "name",[
                'label' => "Prénom",
                'class' => ["formRow"],
                'placeholder' => "Votre prénom...",
                'required' => true
            ]));

            $form->addInput(new Input("text", "lname", "lastname",[
                'label' => "Nom",
                'class' => ["formRow"],
                'placeholder' => "Votre nom...",
                'required' => true
            ]));

            $form->addInput(new Input("text", "pseudo", "pseudo",[
                'label' => "Pseudo",
                'class' => ["formRow"],
                'placeholder' => "Votre pseudonyme..."
            ]));

            $form->addInput(new Input("date", "birth", "birthdate",[
                'label' => "Date de naissance",
                'class' => ["formRow"],
                'min' => "1950-01-01",
                'max' => "2010-12-31",
                'required' => true
            ]));

            $form->addInput(new Input("text", "address", "address",[
                'label' => "Adresse",
                'class' => ["formRow"],
                'placeholder' => "Votre adresse...",
                'required' => true
            ]));

            $form->addInput(new Input("number", "Cpost", "postal",[
                'label' => "Code postal",
                'class' => ["formRow"],
                'placeholder' => "Votre code postal...",
                'required' => true
            ]));

            $form->addInput(new Input("text", "city", "town",[
                'label' => "Ville",
                'class' => ["formRow"],
                'placeholder' => "Votre ville...",
                'required' => true
            ]));

            $form->addInput(new Input("select","country", "country",[
                'label' => "Pays",
                'class' => ["formRow"],
                'subList' => [
                    ['value' => null, 'content' => "Votre pays...", 'id' => "select_default"],
                    ['value' => "south_africa", 'content' => "Afrique du Sud"],
                    ['value' => "german", 'content' => "Allemagne"],
                    ['value' => "australia", 'content' => "Australie"],
                    ['value' => "brasil", 'content' => "Bresil"],
                    ['value' => "canada", 'content' => "Canada"],
                    ['value' => "china", 'content' => "Chine"],
                    ['value' => "spanish", 'content' => "Espagne"],
                    ['value' => "usa", 'content' => "Etat-Unis"],
                    ['value' => "france", 'content' => "France"],
                    ['value' => "Italia", 'content' => "Italie"],
                    ['value' => "japan", 'content' => "Japon"],
                    ['value' => "mexico", 'content' => "Mexique"],
                    ['value' => "russia", 'content' => "Russie"],
                ]
            ]));

            $form->addInput(new Input("radio", "sexe", "sexe",[
                'label' => 'Sexe',
                'class' => ["formRow"],
                'subList' => [
                    ['value' => "male", 'content' => "Homme"],
                    ['value' => "female", 'content' => "Femme"],
                    ['value' => "none", 'content' => "Non renseigné"]
                ],
                'required' => true
            ]));

            $form->addInput(new Input("file", "Pic", "Image",[
                'label' => "Photo de profil",
                'class' => ["formRow"],
                'function' => 'onchange="display_pic(event)"'
            ]));

            $form->addInput(new Input("listCheckBox",null,"Hobbies",[
                'label' => "Loisirs",
                'class' => ["formRow"],
                'subList' => [
                    ['id' => "cooking", 'value' => "cuisine", 'content' => "Cuisine"],
                    ['id' => "videogames", 'value' => "jeux-video", 'content' => "Jeux-video"],
                    ['id' => "reading", 'value' => "lecture", 'content' => "Lecture"],
                    ['id' => "music", 'value' => "musique", 'content' => "Musique"],
                    ['id' => "sport", 'value' => "sport", 'content' => "Sport"],
                    ['id' => "others", 'value' => "autres", 'content' => "Autres..."]
                ]
            ]));

            $form->addInput(new Input("color", "favcolor", "color",[
                'label' => "Couleur préférée",
                'class' => ["formRow"],
            ]));

            $form->addInput(new Input("checkbox", "terms", "Terms", [
                'label' => 'J\'ai lu et j\'accepte <a href="#">les conditions générales d\'utilisation</a> et <a href="#">la politique d\'utilisation des données</a>.',
                'div' => true
            ]));

            $form->addInput(new Input("checkbox", "newsletter", "Newsletter", [
                'label' => "Je souhaite recevoir la newsletter.",
                'div' => true
            ]));
            
            echo $form->toHTML();
            
            ?>

            <?php include 'modules/mainAside.php'; ?>
        </div>
        <div class="side"></div>
    </main>
    <?php include 'modules/footer.php'; ?>

    <script src="js/js.js"></script>
    <script>
        /*
            Fonctions utilisées uniquement sur la page Inscription
        */

        // CONST
        const C_PASSWORD = document.getElementById('Cpassword')
        const DATE = document.getElementById('birth')
        const ZIP_CODE = document.getElementById('Cpost')
        const REGEX_DATE = /^(19[5-9][0-9]|20(0[0-9]|10))\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
        const REGEX_ZIP = /^([0-9]){5}$/;

        const SUBMIT = document.querySelector('button.Contact_button[type=submit]');
        const RESET = document.querySelector('button.Contact_button[type=reset]');
        const tab_verif = [ // Input à vérifier
            document.getElementById('email'),
            document.getElementById('password'),
            document.getElementById('Cpassword'),
            document.getElementById('fname'),
            document.getElementById('lname'),
            document.getElementById('birth'),
            document.getElementById('address'),
            document.getElementById('Cpost'),
            document.getElementById('city'),
        ];

        const tab_verifS = [ // Radio à vérifier
            document.getElementById('male'),
            document.getElementById('female'),
            document.getElementById('none'),
        ];

        // Methode de validation de la page Inscription
        function validation() {
            function set_check() {
                for (let i = 0; i < tab_verif.length; i++) {
                    if (String(tab_verif[i].value).length > 0 && tab_verif[i].style.border == "1px solid rgb(0, 177, 0)") {
                        nb++;
                    }
                }

                if (nb == tab_verif.length) {
                    return true;
                } else {
                    return false;
                }
            }

            let nb = 0;
            let check = set_check();
            let checkS = false;

            if (tab_verifS[0].checked == true ||
                tab_verifS[1].checked == true ||
                tab_verifS[2].checked == true
            ) {
                checkS = true;
            } else {
                checkS = false;
            }

            if (check && checkS) {
                //console.log("submit unlocked");
                SUBMIT.removeAttribute('disabled')
            } else {
                SUBMIT.setAttribute('disabled', true)
            }
        };

        for (let i = 0; i < tab_verifS.length; i++) {
            tab_verifS[i].addEventListener('change', validation);
        }

        // Verifie la correspondance avec le mot de passe
        function Cpasswordfunction() {
            if (PASSWORD.value == C_PASSWORD.value && C_PASSWORD.value != "") {
                document.getElementById('Cpassword').style.border = "solid 1px #00b100";
                ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');

            } else {
                document.getElementById('Cpassword').style.border = "solid 1px red";
                ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
            }
            validation()
        };

        C_PASSWORD.addEventListener('keyup', Cpasswordfunction);

        // Vérifie si la date est entre 1950 et 2010 
        function verifDate(date) {
            if (REGEX_DATE.test(date.value)) {
                date.setAttribute("style", "border: solid 1px #00b100");
                ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
            } else {
                date.setAttribute("style", "border: solid 1px red");
                ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
            }
            validation()
        }

        DATE.addEventListener('change', verifDate.bind(null, DATE))

        function verifZip(zip) {
            if (REGEX_ZIP.test(zip.value)) {
                zip.setAttribute("style", "border: solid 1px #00b100");
                ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
            } else {
                zip.setAttribute("style", "border: solid 1px red");
                ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
            }
            validation()
        }

        ZIP_CODE.addEventListener('keyup', verifZip.bind(null, ZIP_CODE))

        // Permet d'afficher l'image contenu dans l'input "Photo de profil"
        function display_pic(event) {
            console.log(event.target.files[0])
            try {
                let output = document.getElementById('display');
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function() {
                    URL.revokeObjectURL(output.src)
                }
                document.getElementById('Pic_div').style.display = 'flex';
            } catch (error) {
                document.getElementById('Pic_div').style.display = 'none';
            }
        };

        // Efface l'image afficher quand on appuie sur reset
        function erase_pic() {
            document.getElementById('Pic_div').style.display = 'none';
            validation();
        };

        RESET.addEventListener('click', erase_pic);

        // Interdit l'utilisateur de copier son mot de passe
        function interdit() {
            alert("INTERDIT : vous ne pouvez faire un copier-coller de votre mot de passe");
            C_PASSWORD.value = "";
        }

        C_PASSWORD.addEventListener('paste', interdit);

        // Fait apparaitre un pop-up informatif
        function info() {
            if (PASSWORD.value != C_PASSWORD.value && C_PASSWORD.value != "" && PASSWORD.value != "") {
                alert("ATTENTION : la confirmation n'est pas identique à votre mot de passe");
            }
        }

        C_PASSWORD.addEventListener('blur', info);

        // Signale l'obligation de lire les GCUs
        function warningGCUs() {
            if (SUBMIT.getAttribute('disabled') == null && document.getElementById('terms').checked == false) {
                alert("Veuillez confirmer avoir lu les conditions générales d'utilisation et la politique d'utilisation des données.");
            }
        }

        SUBMIT.addEventListener('click', warningGCUs);
    </script>
</body>

</html>