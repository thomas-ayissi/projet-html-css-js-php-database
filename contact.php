<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Contact";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Contact">
        <div class="side"></div>
        <div id="content">
            <p id="introduction">Si vous souhaitez nous contacter, entrer les informations demandées et ensuite écrivez-nous un message dans la zone prévu à cet effet. <br>Vous pouvez également <a href="#N-contact">aller au bas de la page</a> pour récupérer nos coordonnées et nos horaires si vous préférer nous contacter directement.</p>
            <?php
            
            if (isset($_SESSION['yourAccounts']) && !empty($_SESSION['yourAccounts'])) {
                $fname = ($_SESSION['yourAccounts']->getName());
                $lname = ($_SESSION['yourAccounts']->getLastName());
                $email = ($_SESSION['yourAccounts']->getEmail());
            }
            
            $form = new Form("Message", "/returnForm.php", null, "post");
            $form->setFieldset(true);

            $form->setReset(null, false, "Effacer");
            $form->addClassReset("Contact_button");

            $form->setSubmit("submit_button", true, "Envoyer");
            $form->addClassSubmit("Contact_button");

            $form->addInput(new Input("text", "fname", "name",[
                'label' => "Prénom",
                'class' => ["formRow"],
                'placeholder' => "Votre prénom...",
                'required' => true,
                'autofocus' => true,
                'value' => $fname ?? NULL
            ]));

            $form->addInput(new Input("text", "lname", "lastname", [
                'label' => "Nom",
                'class' => ["formRow"],
                'placeholder' => "Votre nom...",
                'required' => true,
                'value' => $lname ?? NULL
            ]));

            $form->addInput(new Input("email", "email", "email", [
                'label' => "Adresse Email",
                'class' => ["formRow"],
                'placeholder' => "Votre adresse mail...",
                'required' => true,
                'value' => $email ?? NULL
            ]));

            $form->addInput(new Input("tel", "phoneNum", "phone",[
                'label' => "Numero de téléphone",
                'class' => ["formRow"],
                'placeholder' => "Votre numéro de téléphone...",
                'pattern' => "[0-9]{10}",
                'required' => true
            ]));

            $form->addInput(new Input("select","country", "country",[
                'label' => "Pays",
                'class' => ["formRow"],
                'subList' => [
                    ['value' => null, 'content' => "Votre pays...", 'id' => "select_default"],
                    ['value' => "south_africa", 'content' => "Afrique du Sud"],
                    ['value' => "german", 'content' => "Allemagne"],
                    ['value' => "australia", 'content' => "Australie"],
                    ['value' => "brasil", 'content' => "Bresil"],
                    ['value' => "canada", 'content' => "Canada"],
                    ['value' => "china", 'content' => "Chine"],
                    ['value' => "spanish", 'content' => "Espagne"],
                    ['value' => "usa", 'content' => "Etat-Unis"],
                    ['value' => "france", 'content' => "France"],
                    ['value' => "Italia", 'content' => "Italie"],
                    ['value' => "japan", 'content' => "Japon"],
                    ['value' => "mexico", 'content' => "Mexique"],
                    ['value' => "russia", 'content' => "Russie"],
                ]
            ]));

            $form->addInput(new Input("text", "subject", "sujet",[
                'label' => "Objet",
                'class' => ["formRow"],
                'placeholder' => "Le sujet de votre message...",
                'required' => true
            ]));

            $form->addInput(new Input("textarea", "message", "content",[
                'label' => "Contenu du message",
                'class' => ["formRow"],
                'placeholder' => "Entrer votre message ici...",
                'required' => true
            ]));
            echo $form->toHTML();
            ?>

            <?php include 'modules/mainAside.php'; ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>


    <script src="js/js.js"></script>
    <script>
        /*
            Fonctions utilisées uniquement sur la page Contact
        */

        // CONST
        const REGEX_PHONE = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;


        const PHONE = document.getElementById('phoneNum')
        const SUBMIT = document.querySelector('#submit_button')

        const tab_verif = [ // Input à vérifier
            document.getElementById('fname'),
            document.getElementById('lname'),
            document.getElementById('email'),
            document.getElementById('phoneNum')
        ];

        // Méthode de vérifiaction de la page Contact
        // Dévérouille le bounton "submit" si tous les champs sont valide.
        function validation() {
            function set_check() {
                for (let i = 0; i < tab_verif.length; i++) {
                    if (String(tab_verif[i].value).length > 0 && tab_verif[i].style.border == "1px solid rgb(0, 177, 0)") {
                        nb++;
                    }
                }

                if (nb == tab_verif.length) {
                    return true;
                } else {
                    return false;
                }
            }

            let nb = 0;

            if (set_check()) {
                //console.log("unlock");
                SUBMIT.removeAttribute('disabled')
            } else {
                SUBMIT.setAttribute('disabled', true)
            }
        };


        // Vérifie si le numéro de téléphone est correctement écrit.
        function PhoneNBfunction(p) {
            if (REGEX_PHONE.test(p.value) && p.value != "") {
                p.style.border = "solid 1px #00b100";
                ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
            } else {
                p.style.border = "solid 1px red";
                ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
                ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
            }
            validation()
        };

        PHONE.addEventListener('keyup', PhoneNBfunction.bind(null, PHONE));

        try {
            veriffunction(document.getElementById('fname'))
            veriffunction(document.getElementById('lname'))
            emailfunction()
        } catch (error) {
            //console.error(error)
        }

        <?php
            if(isset($_SESSION['message-ok'])){
                unset($_SESSION['message-ok']);
                echo 'alert("message envoyé")';
            }
        ?>
    </script>
</body>

</html>