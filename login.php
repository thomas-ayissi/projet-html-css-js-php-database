<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Connexion";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Connexion">
        <div class="side"></div>
        <div id="content">
            <?php


            $form = new Form("Connexion", "/returnForm.php", null, "get");

            $form->setSubmit("submit_button", true, "Se connecter");
            $form->addClassSubmit("Contact_button");

            $form->addInput(new Input("email", "email", "email",[
                'label' => "Adresse Email",
                'class' => ["formRow","specialFocus"],
                'required' => true,
                'autofocus' => true,
                'displayOption' => true
            ]));

            $form->addInput(new Input("password", "password", "password", [
                'label' => "Mot de passe",
                'class' => ["formRow","specialFocus"],
                'required' => true,
                'displayOption' => true
            ]));

            $form->addInput(new Input("checkbox", null, "remember-me", [
                'label' => "Se souvenir de moi"
            ]));

            $form->addInput(new Input("link",null,null,[
                'class' => ["Contact_button"],
                'href' => "/inscription.php",
                'content' => "Créer un compte"
            ]));

            echo $form->toHTML();

            ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="/js/js.js"></script>
    <script>
        try {
            const EMAIL = document.getElementById('email');
        } catch (error) {
            console.error('constante déjà déclaré')
        }
        const PW = document.getElementById('password');
        const SUBMIT = document.getElementById('submit_button');

        const tab_verif = [ // Input à vérifier
            document.getElementById('email'),
            document.getElementById('password')
        ];

        function isCompleted(input) {
            if (input.value != "") {
                input.classList.add('completed');
            } else {
                input.classList.remove('completed');
            }
        }

        EMAIL.addEventListener('change', isCompleted.bind(null, EMAIL));
        PW.addEventListener('change', isCompleted.bind(null, PW));

        function validation() {
            function set_check() {
                for (let i = 0; i < tab_verif.length; i++) {
                    if (String(tab_verif[i].value).length > 0 && tab_verif[i].style.border == "1px solid rgb(0, 177, 0)") {
                        nb++;
                    }
                }

                if (nb == tab_verif.length) {
                    return true;
                } else {
                    return false;
                }
            }

            let nb = 0;

            if (set_check()) {
                SUBMIT.removeAttribute('disabled')
            } else {
                SUBMIT.setAttribute('disabled', true)
            }
        };
    </script>

</body>

</html>