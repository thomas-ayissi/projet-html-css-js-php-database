<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");

$_SESSION = array();
session_destroy();

setcookie("userEmail", "", time() - 3600, "/");

header('Location: index.php');
exit();
