<?php
const TEL = "+33 06 99 99 99 99";
const MAIL = "monsite@gmail.com";
const POST_ADDRESS = "4 Place du Panthéon, Paris";

function supportWEBP()
{
    if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
        return true;
    }
    return false;
}

function image_WEBP($imagePATH)
{
    if (!supportWEBP() && (substr($imagePATH, -5) == ".webp")) {
        return $imagePATH . '.jpeg';
    }
    return $imagePATH;
}
