<?php


try {
    $pdo = new PDO('sqlite:../database.db');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bool = "DB connected";

    try { //!!! ONLY FOR SQLITE !!!
        $pdo->beginTransaction();
        $sth = $pdo->prepare("CREATE TABLE IF NOT EXISTS users (
            id int(11) NOT NULL PRIMARY KEY ,
            email varchar(255) DEFAULT NULL,
            name varchar(255) DEFAULT NULL,
            lastname varchar(255) DEFAULT NULL,
            password varchar(255) DEFAULT NULL,
            town varchar(255) DEFAULT NULL,
            postal varchar(10) DEFAULT NULL,
            address varchar(255) DEFAULT NULL,
            active varchar(255) DEFAULT NULL,
            updated datetime DEFAULT NULL
        );");

        $sth->execute();

        $sth = $pdo->prepare("CREATE TABLE IF NOT EXISTS messages (
            id int(11) NOT NULL PRIMARY KEY,
            name varchar(255) DEFAULT NULL,
            lastname varchar(255) DEFAULT NULL,
            email varchar(255) DEFAULT NULL,
            country varchar(255) DEFAULT NULL,
            sujet varchar(255) DEFAULT NULL,
            content varchar(500) DEFAULT NULL,
            sendingDate datetime DEFAULT NULL
        );");

        $sth->execute();

        $sth = $pdo->prepare("CREATE TABLE IF NOT EXISTS articles (
            id int(11) NOT NULL PRIMARY KEY,
            title varchar(255) DEFAULT NULL,
            author varchar(255) DEFAULT NULL,
            description varchar(255) DEFAULT NULL,
            content varchar(5000) DEFAULT NULL,
            image varchar(250) DEFAULT NULL,
            publicationDate datetime DEFAULT NULL
          );");

        $sth->execute();

        $pdo->commit();
    } catch (Exception $e) {
        //throw $th;
        echo $e->getLine() . ":" . $e->getMessage();
        $pdo->rollBack();
    }
} catch (Exception $e) {
    //echo $e->getMessage();
    //phpinfo();
    echo "Impossible d'accéder à la base de données SQLite : " . $e->getMessage();
    die();
}
