<?php
// include this file in returnForm.php

try {
    $pdo->beginTransaction();
    if (!accountAlreadyExists($pdo)) {
        $encryptedPW = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $nbUsers = $pdo->query("SELECT count(id) FROM users")->fetchColumn();
        $sth = $pdo->prepare("INSERT INTO users (id, email, name, lastname, password, town, postal, address, active, updated) 
                                    VALUES 
                                    (" . $pdo->quote($nbUsers + 1) . ",
                                    " . $pdo->quote($_POST['email']) . ", 
                                    " . $pdo->quote($_POST['name']) . ", 
                                    " . $pdo->quote($_POST['lastname']) . ", 
                                    " . $pdo->quote($encryptedPW) . ", 
                                    " . $pdo->quote($_POST['town']) . ", 
                                    " . $pdo->quote($_POST['postal']) . ", 
                                    " . $pdo->quote($_POST['address']) . ", 
                                    1,'" . date("Y-m-d H:m:s") . "');");
        $sth->execute();

        $pdo->commit();

        $user = new User(
            $_POST['email'],
            $_POST['name'],
            $_POST['lastname'],
            $_POST['town'],
            $_POST['postal'],
            $_POST['address'],
            1
        );
        $user->setID($nbUsers + 1);
        
        $_SESSION['yourAccounts'] = $user;
        $_SESSION['Admin'] = true;
    } else {
        header('Location: inscription.php');
        exit();
    }
} catch (Exception $e) {
    echo $e->getLine() . " " . $e->getMessage();
    $pdo->rollBack();
    echo " echec de l'envoie des données";
    die();
}