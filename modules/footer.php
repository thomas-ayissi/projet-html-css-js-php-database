<?php include_once($_SERVER["DOCUMENT_ROOT"] . '/include/config.php');

    if(isset($_SESSION['recentArticles3'])|| !empty($_SESSION['recentArticles3'])){
        $recent3 = $_SESSION['recentArticles3'];
        unset($_SESSION['recentArticles3']);
    }
    else{
        $die();
    }

    function footerArticles($recent3){
        $string = "";
        for ($i = 0; $i < count($recent3); $i++){
            $string .= '<li id="li'.($i+1).'"><a id="link'.($i+1).'" href="/blog.php?article=' . $recent3[$i]['id'] . '">'.substr($recent3[$i]['title'],0,20).'...</a></li>';
        }
        return $string;
    }
?>
<footer>
    <div id="article_contact">
        <div id="article">
            <ul id="article-content">
                <?= footerArticles($recent3); ?>
            </ul>
            <p>Les derniers articles<span></span></p>
        </div>
        <a id="contact-footer-button" href="/contact.php">Contact<span></span></a>
        <div id="contact-info">
            <span></span>
            <ul>
                <li>
                    <a href="#"><strong>Numéro de téléphone :</strong> <?= TEL ?></a>
                </li>
                <li>
                    <a href="#"><strong>Adresse mail : </strong> <?= MAIL ?></a>
                </li>
            </ul>
        </div>
    </div>
    <p id="Copyright"><span></span>CopyRight © / 2021-2022 / Alexis FORESTIER</p>
</footer>

<?php
if (!empty($_SESSION)&&isset($_SESSION['yourAccounts'])) {
    echo "<script>\n";
    echo "document.querySelector('#connect div').classList.add('subContentConnect');\n";
    echo "</script>\n";
}
