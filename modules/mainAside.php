<!--Contacter-nous-->
<div id="fixN-contact">
    <aside id="N-contact">
        <span><span>&#128222;</span> Nous contacter <span>&#128222;</span></span>
        <div>
            <section>
                <h4>Nos Coordonnées</h4>
                <ul>
                    <li><strong>Numéro de téléphone :</strong> <?= TEL ?></li>
                    <li><strong>Adresse mail : </strong> <?= MAIL ?></li>
                    <li><strong>Adresse : </strong> <?= POST_ADDRESS ?></li>
                </ul>
            </section>
            <hr>
            <section>
                <h4>Horaires d'ouverture</h4>
                <p>Du <strong>lundi</strong> au <strong>vendredi</strong> de 9h à 17h</p>
                <p>Les <strong>Week-ends</strong> de 8h30 à 12h30</p>
            </section>
        </div>
    </aside>
</div>