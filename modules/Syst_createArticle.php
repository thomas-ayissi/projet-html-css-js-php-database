<?php
// include this file in returnForm.php

try {
    $pdo->beginTransaction();

    if (!articleAlreadyExists($pdo)) {
        $type = explode('/', $_FILES["Image"]['type'])[1];
        $nbArticles = $pdo->query("SELECT count(id) FROM articles")->fetchColumn();
        $date = date("YmdHms");
        $date2 = date("Y-m-d H:m:s");

        if (!empty($_FILES['Image'])) {
            move_uploaded_file($_FILES["Image"]['tmp_name'], $currentPath . "\img\articles\image" . $date . "." . $type);
        }

        $sth = $pdo->prepare("INSERT INTO articles (id, title, author, description, content, image, publicationDate) 
                                VALUES 
                                (" . $pdo->quote($nbArticles) . ", 
                                " . $pdo->quote($_POST['title']) . ", 
                                " . $pdo->quote($_POST['author']) . ", 
                                " . $pdo->quote($_POST['description']) . ",  
                                " . $pdo->quote($_POST['content']) . ", 
                                " . $pdo->quote("/img/articles/image" . $date . "." . $type) . ",
                                " . $pdo->quote($date2) . ");");
        $sth->execute();



        $pdo->commit();
    } else {
        $pdo->rollBack();
        redirect('createArticle.php');
    }
} catch (Exception $e) {
    $pdo->rollBack();
    echo $e->getLine() . ":" . $e->getMessage();
    echo " echec de l'envoie de l'article";
    die();
}
