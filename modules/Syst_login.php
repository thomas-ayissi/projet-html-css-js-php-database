<?php
// include this file in returnForm.php

try {
    $string = $pdo->quote($_GET['email']);
    $sql = "SELECT * 
                    FROM users 
                    WHERE email = " . $string . ";";


    $row = $pdo->query($sql)->fetch();
    $user = new User(
        $row['email'],
        $row['name'],
        $row['lastname'],
        $row['town'],
        $row['postal'],
        $row['address'],
        $row['active'],
    );
    $user->setID($row['id']);
    $password = $row['password'];


    if ($user->getEmail() !=  "" && password_verify($_GET['password'], $password)) {
        $_SESSION['yourAccounts'] = $user;
        $_SESSION['Admin'] = true;
        if (isset($_GET['remember-me'])) {
            setcookie("userEmail", $user->getEmail(), time() + 86400, "/");
        }
    } else {
        header('Location: login.php');
        exit();
    }
} catch (Exception $e) {
    echo $e->getMessage();
    //echo "probleme d'execution de commande sql";
    header('Location: login.php');
    exit();
}
