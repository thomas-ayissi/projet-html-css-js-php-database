<?php

if (empty($_SESSION) || !isset($_SESSION['Admin'])){
    header('Location: index.php');
    exit();
}

// else user can access the page