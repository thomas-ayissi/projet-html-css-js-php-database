<?php
// include this file in returnForm.php

try {
    $pdo->beginTransaction();
    $nbMessages = $pdo->query("SELECT count(id) FROM messages")->fetchColumn();
    $sth = $pdo->prepare("INSERT INTO messages (id, name, lastname, email, country, sujet, content, sendingDate) 
                            VALUES 
                            (" . $pdo->quote($nbMessages) . ", 
                            " . $pdo->quote($_POST['name']) . ", 
                            " . $pdo->quote($_POST['lastname']) . ", 
                            " . $pdo->quote($_POST['email']) . ",  
                            " . $pdo->quote($_POST['country']) . ", 
                            " . $pdo->quote($_POST['sujet']) . ", 
                            " . $pdo->quote($_POST['content']) . ", 
                            " . $pdo->quote(date("Y-m-d H:m:s")) . ");");
    $sth->execute();

    $pdo->commit();
} catch (Exception $e) {
    $pdo->rollBack();
    echo $e->getLine() . ":" . $e->getMessage();
    echo " echec de l'envoie du message";
    die();
}
