<?php

$navLinks = array(
    "href=\"/index.php\"",
    "href=\"/presentation.php\"",
    "href=\"/contact.php\"",
    "href=\"/blog.php\"",
    "href=\"/inscription.php\"",
    "href=\"/chiffres.php\""
);


foreach ($navLinks as $key => $value) {
    if (explode("=", $value)[1] == "\"" . $_SERVER['PHP_SELF'] . "\"") {
        $navLinks[$key] = "class=\"active\" " . $value;
    }
}

function isConnected()
{
    if (!empty($_SESSION) && isset($_SESSION['yourAccounts'])) {
        try {
            $welcome = $_SESSION['yourAccounts']->getName() . " " . $_SESSION['yourAccounts']->getLastName();
            return '
                <li id="connect">
                <p id="welcome">Bienvenue ' . $welcome . '</p>
                    
                    <div class="subContent Connect">
                    <a href="/account.php">Votre compte</a>
                        <a href="/logout.php">Se déconnecter</a>
                    </div>
                </li>';
        } catch (\Throwable $th) {
            die();
        }
    }
    return '
        <li id="connect">
            <a href="/inscription.php">Inscription</a>
            <div class="subContent">
                <a href="/login.php">Connexion</a>
            </div>
        </li>
        ';
}

function linksArticles($pdo)
{
    $nbArticles = $pdo->query("SELECT count(id) FROM articles")->fetchColumn() ?? 0;

    $sql = "SELECT * FROM articles LIMIT " . ($nbArticles - 5) . ",5";
    $row = $pdo->query($sql)->fetchAll();
    $row = array_reverse($row);

    $_SESSION['recentArticles3']=[
        $row[0],
        $row[1],
        $row[2]
    ];

    $links="";
    foreach ($row as $article) {
        $links.='<a href="/blog.php?article=' . $article['id'] . '">'.$article['title'].'</a>';
    }
    return $links;
}

?>

<header>
    <div>
        <img id="logo" src="/img/SCPlogo.png" alt="Logo">
        <a href="index.html">
            <h1>Mon Site</h1>
        </a>
    </div>
    <nav>
        <input type="checkbox" id="boutonmenu" name="menubutton">
        <span id="boutonmenu_span"></span>
        <ul>
            <li><a <?= $navLinks[0]; ?>>Accueil</a></li>
            <li><a <?= $navLinks[1]; ?>>Présentation</a></li>
            <li><a <?= $navLinks[2]; ?>>Contact</a></li>
            <li id="blog">
                <a <?= $navLinks[3]; ?>>Blog</a>
                <div class="subContent">
                    <?= linksArticles($pdo); ?>
                </div>
            </li>
            <?= isConnected(); ?>
            <li><a <?= $navLinks[5]; ?>>Les chiffres</a></li>
        </ul>
    </nav>
</header>