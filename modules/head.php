<head>
    <title><?= $name; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" type="image/ico" href="/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once($_SERVER["DOCUMENT_ROOT"] . '/include/config.php') ?>
</head>