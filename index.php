<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Accueil";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Accueil">

        <div class="side"></div>
        <div id="content">
            <section>
                <h2>Bienvenue</h2>
                <p id="intro">Phasellus vel magna eu justo congue convallis. Suspendisse sem urna, venenatis a accumsan a, consequat ut sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae enim orci. Aliquam pharetra tellus a orci lacinia, posuere malesuada enim mattis. <strong>Phasellus volutpat</strong> lacinia sollicitudin. Integer semper vitae tellus ac tempus. Curabitur auctor hendrerit dui, nec ultrices sem pellentesque in. Sed bibendum gravida felis in porttitor. Pellentesque iaculis ac enim a lobortis. Nulla porttitor ipsum vitae dapibus volutpat. Morbi mattis ipsum non hendrerit vestibulum. Phasellus porta nibh id nulla tempor, sit amet feugiat dui viverra. Phasellus est turpis, vehicula et tortor eu, facilisis lacinia dui. Aliquam erat volutpat.</p>
            </section>
            <section>
                <h2>Qui somme-nous ?</h2>
                <div id="blur">
                    <img src="img/city.jpg" alt="une forêt">
                </div>
                <p>Curabitur viverra nunc ac fermentum ullamcorper. Nulla id ex a ipsum vulputate fermentum. Etiam a nisl id quam vulputate vestibulum. <strong>Suspendisse semper orci sapien</strong>, sit amet ultrices urna pharetra sit amet. Aliquam quis faucibus nisl, vitae blandit orci. Maecenas erat mi, varius eget facilisis nec, vestibulum eu sem. Morbi maximus aliquet sapien, quis posuere odio rutrum id. Suspendisse est neque, condimentum nec convallis in, feugiat vel diam. Curabitur feugiat pharetra elit, sit amet eleifend ipsum scelerisque sit amet. Cras in <strong>quam eu purus tincidunt</strong> gravida et quis massa. Ut sagittis suscipit tincidunt. Praesent scelerisque dolor purus, sed vestibulum neque lobortis nec. Donec suscipit odio a tortor maximus, quis luctus velit auctor. Integer ac felis feugiat, bibendum ligula sollicitudin, sagittis est. Mauris placerat, arcu vitae iaculis tempus, odio mi volutpat nibh, sit amet efficitur velit elit ac ipsum. Suspendisse consequat euismod leo, vel viverra ante condimentum ac.<a id="savoir_plus" href="/presentation.php">En savoir plus...</a></p>
            </section>
            <section>
                <h2>Est-ce que votre navigateur prend en charge le WEBP ?</h2>
                <img src=<?= '"' . image_WEBP("img/img_jpg_webp/img-18_800x1.webp") . '"' ?> alt="webp est-il prit en charge ?">
                <?php
                if (supportWEBP()) {
                    echo "<p>Votre navigateur en prend en charge le WEBP.</p>";
                } else {
                    echo "<p>Votre navigateur ne prend pas en charge le WEBP, cette image est en JPG.</p>";
                }
                ?>
            </section>
            <section>
                <h2>Maj du 28/11/2021</h2>
                <img src="/img/maj28-11-21.jpg" alt="mise a jour du 28/11/2021">
                <p>Le formulaire de la page Connexion est généré à partir de classes PHP.</p>
            </section>
            <section>
                <h2>Maj du 27/11/2021</h2>
                <img src="/img/maj27-11-21.jpg" alt="mise a jour du 27/11/2021">
                <p>La page de blog se génère avec les données de la base de données.</p>
                <p>La barre de recherche et la pagination sont fonctionnelles.</p>
                <p>Les 5 articles les plus récents apparaissent dans la navbar et les 3 plus récents dans le footer.</p>
            </section>
            <section>
                <h2>Maj du 24/10/2021</h2>
                <img src="img/maj24-10-21.jpg" alt="mise a jour du 24 octobre 2021">
                <p>Changement du contenu de la page d'accueil pour réduire le nombre d'animations et présenter les fonctions ajoutées au site.</p>
            </section>
            <section>
                <h2>Maj du 16/10/2021</h2>
                <img src="img/Switch_theme.jpg" alt="changer de theme">
                <p>En cliquant sur le logo du site, vous pouvez désormais changer le thème entre lumineux et sombre. Le thème de votre système est automatiquement appliqué au site quand vous arriver dessus.</p>
            </section>
            <section>
                <h2>Maj du 01/10/2021</h2>
                <img src="img/maj01-10-21.jpg" alt="mise a jour du 01/10/2021">
                <p>Vérification des données entrées dans les inputs des pages inscription et contact. Une bordure de couleur apparait autour de l'input pour montrer si le contenu est correct ou non. Quand tous les inputs sont corrects le bouton "envoyer" devient actif.</p>
                <p>Nouveau design pour les inputs de type checkbox !</p>
            </section>
            <section>
                <h2>Fonctions et ajouts en plus</h2>
                <img src="img/Others_features.jpg" alt="tous les autres fonctions ajoutées">
                <ul>
                    <li>Ajout d'une icone pour le site</li>
                    <li>La couleur de selection du texte a été changée</li>
                    <li>Le design des inputs de type radio a été changé</li>
                    <li>Ajout de différentes animations sur le footer pour rendre les pages plus dynamique</li>
                    <li>Ajout d'une icone loupe animée sur la page blog</li>
                    <li>Ajout d'une icone téléphone animée sur les pages contact et inscription</li>
                    <li>Dans la page inscription, l'input de type image peut afficher l'image qu'il contient</li>
                    <li>Le design des barre de défilement a été changer (uniquement pour les navigateurs webkit (ex:chrome))</li>
                    <li>Dans la page présentation, les valeurs de la partie contextuelle peuvent être automatiquement copier dans le presse-papier</li>
                    <li>Dans le page chiffre, les colonnes du tableau peuvent être séléctionées via la partie contextuelle</li>
                </ul>
            </section>
        </div>
        <div class="side"></div>

    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="js/js.js"></script>
</body>

</html>