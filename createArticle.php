<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/adminOnly.php");
?>

<?php
function getNameLastName()
{
    if (isset($_SESSION['yourAccounts']) && !empty($_SESSION['yourAccounts'])){
        return $_SESSION['yourAccounts']->getName() . " " . $_SESSION['yourAccounts']->getLastName();
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Ajouter un Article";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="createArticle">
        <div class="side"></div>
        <div id="content">
            <?php

            $form = new Form("Ajouter un Article", "/returnForm.php", null, "post");

            $form->setSubmit("submit_button", true, "Ajouter l'article");
            $form->addClassSubmit("Contact_button");
            
            $form->addInput(new Input("text", "articleTitle", "title",[
                'label' => "Titre de l'article",
                'class' => ["formRow","specialFocus"],
                'required' => true,
                'autofocus' => true,
                'displayOption' => true
            ]));

            $form->addInput(new Input("text", "articleAuthor", "author", [
                'label' => "Auteur de l'article",
                'class' => ["formRow","specialFocus"],
                'required' => true,
                'displayOption' => true,
                'value' => getNameLastName()
            ]));

            $form->addInput(new Input("text", "articleDesc", "description", [
                'label' => "Description de l'article",
                'class' => ["formRow","specialFocus"],
                'displayOption' => true
            ]));

            $form->addInput(new Input("textarea","articleContent", "content",[
                'label' => "Titre de l'article",
                'class' => ["formRow"],
                'maxlength' => 5000,
                'p' => ['*Utiliser les balises HTML pour ajouter de la forme à votre contenu',
                        '*max : 5000 caractères'],
                'required' => true
            ]));

            $form->addInput(new Input("file", "articleImage", "Image",[
                'label' => "Illustation de l'article",
                'class' => ["formRow"],
                'function' => 'onchange="display_pic(event)"'
            ]));

            $form->addInput(new Input("link",null,null,[
                'class' => ["Contact_button"],
                'href' => "/blog.php",
                'content' => "Annuler"
            ]));
            echo $form->toHTML();
            ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="/js/js.js"></script>
    <script>
        const SUBMIT = document.getElementById('submit_button');
        const AUTHOR = document.getElementById('articleAuthor');
        const DESC = document.getElementById('articleDesc');
        const CONTENT = document.getElementById('articleContent');
        const LIMIT = document.querySelector('#CONT p:nth-of-type(2)');
        const tab_verif = [ // Input à vérifier
            document.getElementById('articleTitle'),
            document.getElementById('articleContent')
        ];

        function isCompleted(input) {
            if (input.value != "") {
                input.classList.add('completed');
                return true;
            } else {
                input.classList.remove('completed');
                return false;
            }
        }

        AUTHOR.addEventListener('keyup', isCompleted.bind(null, AUTHOR));
        isCompleted(AUTHOR);
        DESC.addEventListener('keyup', isCompleted.bind(null, DESC));


        function validation() {

            if (isCompleted(tab_verif[0]) && isCompleted(tab_verif[1])) {
                SUBMIT.removeAttribute('disabled')
            } else {
                SUBMIT.setAttribute('disabled', true)
            }
        };

        tab_verif[0].addEventListener('keyup', validation);
        tab_verif[1].addEventListener('keyup', validation);

        function display_pic(event) {
            console.log(event.target.files[0])
            try {
                let output = document.getElementById('display');
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function() {
                    URL.revokeObjectURL(output.src)
                }
                document.getElementById('Pic_div').style.display = 'flex';
            } catch (error) {
                document.getElementById('Pic_div').style.display = 'none';
            }
        };

        let defaultContent = LIMIT.textContent;
        function lengthContent(){
            nbChar = CONTENT.value.length;
            LIMIT.textContent = defaultContent+" ("+nbChar+")";
            if(nbChar>5000){
                LIMIT.style.color = 'red';
            }
            else{
                LIMIT.removeAttribute('style');
            }
        }

        CONTENT.addEventListener('keyup',lengthContent);
    </script>

</body>

</html>