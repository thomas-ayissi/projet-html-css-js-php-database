# Projet HTML-CSS-JS-PHP-DATABASE

Un TP assez gros pour être considéré comme un projet, le but était de créer un site de type blog en revoyant les bases de la programmation web (HTML, CSS, JavaScript). En deuxième partie de ce projet, intégré la partie serveur en PHP (conversion de tous les .html en .php) ainsi qu'une gestion des données avec une base de données et du SQLite.

## Tester le projet

1. Cloner le projet 
2. Télécharger et intégrer la [base de données](https://drive.google.com/file/d/1bQzLPSdyqIlW_fF5RLhAi71SCsdMnuPW/view?usp=sharing) à l'extérieur du répertoire du projet
    - ![](https://cdn.discordapp.com/attachments/804441909891956807/928356994237231185/doc.png)
3. Placer vous dans le répertoire du projet cloner et dans la barre de chemin d'accès taper **"cmd"**
Un terminal s'ouvre, entrer la commande suivante :

```
php -S localhost:80
```

4. Laisser le terminal ouvert et dans la barre de recherche d'un navigateur internet (Chrome, Firefox, Opera...)
veuillez taper **"localhost:80"**.
Vous devriez accéder à l'index du projet.

## Données du projet GitLab original

Projet original disponible sur [IUT-bg GitLab](https://iutbg-gitlab.iutbourg.univ-lyon1.fr/).

**Vous devez posséder le vpn de l'IUT-Lyon1 et un compte de l'université pour accéder au projet original**

Le projet original est privé.

| Commits | 74 |
|---|---|
| Branches | 1 |
| Temps de travail | +200 heures |
| Durée début/fin | 94 jours |
