<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Chiffres";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <div id="MainAside">
        <main id="Chiffres">
            <div class="side"></div>
            <div id="content">

                <table id="Chiffres_table">
                    <thead>
                        <tr>
                            <th id="column_name">Name</th>
                            <th id="column_phone">Phone</th>
                            <th id="column_email">Email</th>
                            <th id="column_address">Address</th>
                            <th id="column_zip">PostalZip</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="Name">Reese Gomez</td>
                            <td data-label="Phone">05 85 22 68 76</td>
                            <td data-label="Email">eleifend.nec@imperdietdictum.edu</td>
                            <td data-label="Address">2755 Nunc St.</td>
                            <td data-label="PostalZip">80178</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Dexter Bush</td>
                            <td data-label="Phone">04 58 47 27 46</td>
                            <td data-label="Email">sit.amet.ornare@nec.com</td>
                            <td data-label="Address">Ap #520-2763 Sapien. Rd.</td>
                            <td data-label="PostalZip">23962</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Charles Bailey</td>
                            <td data-label="Phone">06 72 62 36 77</td>
                            <td data-label="Email">nec.metus@feugiat.com</td>
                            <td data-label="Address">933-2252 Turpis Street</td>
                            <td data-label="PostalZip">15787</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Tyler Romero</td>
                            <td data-label="Phone">04 84 03 58 79</td>
                            <td data-label="Email">pulvinar.arcu@fermentum.org</td>
                            <td data-label="Address">233-1395 Faucibus St.</td>
                            <td data-label="PostalZip">64172</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Zena Gibson</td>
                            <td data-label="Phone">06 84 74 72 81</td>
                            <td data-label="Email">ante.nunc@nuncsedpede.edu</td>
                            <td data-label="Address">Ap #956-9389 Ut, St.</td>
                            <td data-label="PostalZip">93119</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Aretha Hood</td>
                            <td data-label="Phone">05 76 73 24 64</td>
                            <td data-label="Email">etiam@tincidunt.co.uk</td>
                            <td data-label="Address">519-7936 Egestas. Avenue</td>
                            <td data-label="PostalZip">10543</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Mufutau Dodson</td>
                            <td data-label="Phone">03 42 86 67 74</td>
                            <td data-label="Email">interdum.sed@auguemalesuadamalesuada.edu</td>
                            <td data-label="Address">Ap #819-8548 Sed Av.</td>
                            <td data-label="PostalZip">66717</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Kai Joyner</td>
                            <td data-label="Phone">07 18 23 31 92</td>
                            <td data-label="Email">augue.sed@convallis.com</td>
                            <td data-label="Address">127-7223 Luctus St.</td>
                            <td data-label="PostalZip">51343</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Dylan Hanson</td>
                            <td data-label="Phone">01 52 82 36 84</td>
                            <td data-label="Email">nulla.interdum@accumsansed.ca</td>
                            <td data-label="Address">421-5138 Est, Rd.</td>
                            <td data-label="PostalZip">65833</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Louis Banks</td>
                            <td data-label="Phone">09 30 16 99 12</td>
                            <td data-label="Email">lacus.ut@inornare.ca</td>
                            <td data-label="Address">Ap #491-1000 Proin Road</td>
                            <td data-label="PostalZip">80776</td>
                        </tr>
                        <tr>
                            <td data-label="Name">John Schwartz</td>
                            <td data-label="Phone">07 89 77 16 45</td>
                            <td data-label="Email">ligula.nullam@arcu.net</td>
                            <td data-label="Address">9995 Ipsum Av.</td>
                            <td data-label="PostalZip">66206</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Avye Nichols</td>
                            <td data-label="Phone">03 34 18 26 44</td>
                            <td data-label="Email">nulla.eu.neque@crasdictumultricies.org</td>
                            <td data-label="Address">P.O. Box 764, 8902 Neque Rd.</td>
                            <td data-label="PostalZip">42245</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Adena Noble</td>
                            <td data-label="Phone">09 61 72 22 61</td>
                            <td data-label="Email">nunc.sed@necurna.co.uk</td>
                            <td data-label="Address">Ap #826-825 Purus Avenue</td>
                            <td data-label="PostalZip">04736</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Jeremy Sutton</td>
                            <td data-label="Phone">05 39 00 85 51</td>
                            <td data-label="Email">feugiat.metus.sit@id.edu</td>
                            <td data-label="Address">250-9356 Magna Avenue</td>
                            <td data-label="PostalZip">21615</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Wyatt Brady</td>
                            <td data-label="Phone">09 21 71 54 54</td>
                            <td data-label="Email">in.condimentum.donec@euduicum.edu</td>
                            <td data-label="Address">4989 Varius St.</td>
                            <td data-label="PostalZip">88789</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Bree Clay</td>
                            <td data-label="Phone">07 89 68 68 09</td>
                            <td data-label="Email">a.tortor@estvitae.ca</td>
                            <td data-label="Address">879-4802 Mauris Ave</td>
                            <td data-label="PostalZip">35925</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Hoyt Guerra</td>
                            <td data-label="Phone">03 59 87 52 10</td>
                            <td data-label="Email">erat.neque@tortorinteger.org</td>
                            <td data-label="Address">593-8407 Morbi Av.</td>
                            <td data-label="PostalZip">24666</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Erin Bray</td>
                            <td data-label="Phone">08 74 62 34 64</td>
                            <td data-label="Email">vivamus@massa.com</td>
                            <td data-label="Address">790-852 Tellus Road</td>
                            <td data-label="PostalZip">64550</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Tanisha Zamora</td>
                            <td data-label="Phone">02 45 18 09 88</td>
                            <td data-label="Email">ipsum@facilisisloremtristique.org</td>
                            <td data-label="Address">Ap #674-2258 Id Rd.</td>
                            <td data-label="PostalZip">56434</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Avye Harris</td>
                            <td data-label="Phone">01 42 27 85 77</td>
                            <td data-label="Email">ut.lacus.nulla@turpisaliquam.edu</td>
                            <td data-label="Address">Ap #693-9261 Elit Street</td>
                            <td data-label="PostalZip">14834</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Wesley Mitchell</td>
                            <td data-label="Phone">01 69 18 35 28</td>
                            <td data-label="Email">sit.amet@acfacilisis.net</td>
                            <td data-label="Address">P.O. Box 401, 2174 Sed Av.</td>
                            <td data-label="PostalZip">42667</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Kessie Wiley</td>
                            <td data-label="Phone">06 83 19 76 48</td>
                            <td data-label="Email">arcu.curabitur@mialiquam.edu</td>
                            <td data-label="Address">349-9270 Cum Rd.</td>
                            <td data-label="PostalZip">23561</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Erica Cooper</td>
                            <td data-label="Phone">08 75 39 23 63</td>
                            <td data-label="Email">suspendisse.eleifend@ac.edu</td>
                            <td data-label="Address">4070 Magna Rd.</td>
                            <td data-label="PostalZip">32316</td>
                        </tr>
                        <tr>
                            <td data-label="Name">April Prince</td>
                            <td data-label="Phone">02 37 47 37 62</td>
                            <td data-label="Email">nullam.feugiat.placerat@vitaealiquameros.co.uk</td>
                            <td data-label="Address">238-1041 Et St.</td>
                            <td data-label="PostalZip">95058</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Ifeoma Lyons</td>
                            <td data-label="Phone">08 61 78 84 87</td>
                            <td data-label="Email">vitae.diam.proin@uttincidunt.ca</td>
                            <td data-label="Address">P.O. Box 490, 1689 Ornare Road</td>
                            <td data-label="PostalZip">36634</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Kameko Landry</td>
                            <td data-label="Phone">02 38 21 62 26</td>
                            <td data-label="Email">orci.tincidunt@et.org</td>
                            <td data-label="Address">Ap #604-2334 Libero. St.</td>
                            <td data-label="PostalZip">56347</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Nina Cantu</td>
                            <td data-label="Phone">02 22 65 22 24</td>
                            <td data-label="Email">eget.ipsum.donec@aeneanmassa.net</td>
                            <td data-label="Address">3386 Sem St.</td>
                            <td data-label="PostalZip">72638</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Cade Wiley</td>
                            <td data-label="Phone">04 31 87 28 77</td>
                            <td data-label="Email">pharetra.quisque@atsem.com</td>
                            <td data-label="Address">Ap #165-7361 Placerat. Avenue</td>
                            <td data-label="PostalZip">57260</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Dacey Roman</td>
                            <td data-label="Phone">06 61 32 52 47</td>
                            <td data-label="Email">aenean.euismod@aliquamultrices.net</td>
                            <td data-label="Address">Ap #895-1035 Pede. St.</td>
                            <td data-label="PostalZip">11257</td>
                        </tr>
                        <tr>
                            <td data-label="Name">Chaim Mccarty</td>
                            <td data-label="Phone">06 52 55 91 72</td>
                            <td data-label="Email">at.risus@necimperdietnec.co.uk</td>
                            <td data-label="Address">325-3406 Lobortis Av.</td>
                            <td data-label="PostalZip">44366</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="side"></div>
        </main>

        <aside id="Aside_Context">
            <span>&#10133;</span>
            <section>
                <h2>Nos Chiffres</h2>
                <p>Cliquer sur un bloc pour séléctionner la colonne respondante dans le tableau</p>
                <ul>
                    <li>
                        <a href="#"><strong>Name :</strong> Praesent iaculis nisl at blandit euismod. Vestibulum eu ante ac nisl facilisis malesuada semper non augue. Fusce a tincidunt libero. In in turpis neque. Donec eu hendrerit metus, et varius massa. Duis semper sodales justo vitae luctus. Morbi vehicula a lacus sit amet molestie. Aenean et nisl velit. Nulla feugiat luctus nunc non tempus.</a>
                    </li>
                    <li>
                        <a href="#"><strong>Phone :</strong> Quisque molestie, purus a porta rhoncus, dui lectus dapibus dolor, sed elementum est mauris a tortor. Quisque eu eros tortor. Pellentesque non ipsum leo. Aliquam id aliquet est, nec congue tellus. Cras interdum nec ipsum vitae bibendum. Nam a nulla purus. Sed a massa eget nulla sagittis aliquam sed nec diam. Phasellus mollis id dui non lobortis. Cras a lectus sodales, mattis erat in, vestibulum eros. Donec ac ante molestie, molestie lacus vitae, pellentesque lectus.</a>
                    </li>
                    <li>
                        <a href="#"><strong>Email :</strong>Maecenas in malesuada magna. Integer laoreet nisl a lectus dictum consectetur. Phasellus gravida facilisis ligula vitae condimentum. Proin at odio sed nisl maximus volutpat id quis velit. Mauris tincidunt vulputate felis, pulvinar scelerisque felis aliquet sed. Nunc congue auctor suscipit. Nulla pellentesque ornare elit, eget tempus magna blandit in. Donec metus lorem, congue in tristique vel, rhoncus a libero. Cras posuere fermentum pharetra.</a>
                    </li>
                    <li>
                        <a href="#"><strong>Address :</strong> Ut non lacus at quam porttitor feugiat. Fusce dapibus rutrum velit, ut luctus tellus suscipit pharetra. Donec libero urna, sodales quis rutrum sed, elementum in lectus. Sed dictum, risus eu scelerisque dapibus, metus nisl convallis sapien, id finibus velit dui eget nulla. Aliquam erat volutpat. Duis at porttitor turpis. Praesent efficitur lorem urna, nec rutrum elit tristique nec. Phasellus at nisi et ante posuere ornare nec ut nulla. Sed et libero urna. Mauris lacus eros, tempor a hendrerit at, egestas non magna. Proin scelerisque massa vel mi malesuada, rutrum tempor nisl malesuada. Mauris pharetra magna dignissim, venenatis turpis quis, suscipit velit.</a>
                    </li>
                    <li>
                        <a href="#"><strong>PostalZip :</strong> Curabitur velit urna, ultrices sed lacus at, vehicula volutpat ligula. Maecenas nulla felis, porttitor sit amet scelerisque in, varius vitae mi. Phasellus sit amet ipsum molestie, ornare dolor non, finibus nulla. Proin ultricies nibh nec leo laoreet elementum. Aenean maximus sed erat a vehicula. Vestibulum ut risus accumsan, commodo arcu a, semper massa. Donec nec ultrices dui, nec aliquam diam.</a>
                    </li>
                </ul>
            </section>
        </aside>
    </div>

    <?php include 'modules/footer.php'; ?>
    <script src="js/js.js"></script>
    <script>
        const tab_aside = document.querySelectorAll('aside li > a');
        let tab_td;
        let already_highlight = "";

        function highlightColumn(n) {
            clip = tab_aside[n].textContent.split(':');
            if (already_highlight != "" && already_highlight != clip[0]) {
                for (let i = 0; i < tab_td.length; i++) {
                    tab_td[i].removeAttribute("style");
                }
                tab_td = document.querySelectorAll('table tbody td[data-label=' + clip[0] + ']');
                for (let i = 0; i < tab_td.length; i++) {
                    tab_td[i].style.backgroundColor = "var(--select_tr)";
                }
                already_highlight = clip[0];
            } else if (already_highlight != "") {
                for (let i = 0; i < tab_td.length; i++) {
                    tab_td[i].removeAttribute("style");
                }
                already_highlight = "";
            } else {
                tab_td = document.querySelectorAll('table tbody td[data-label=' + clip[0] + ']');
                for (let i = 0; i < tab_td.length; i++) {
                    tab_td[i].style.backgroundColor = "var(--select_tr)";
                }
                already_highlight = clip[0];
            }
            console.log(already_highlight)
        }

        for (let i = 0; i < tab_aside.length; i++) {
            tab_aside[i].addEventListener('click', highlightColumn.bind(null, i));
        }
    </script>
</body>

</html>