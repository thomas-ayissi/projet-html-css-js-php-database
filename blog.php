<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Blog";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <?php include 'modules/blogMain.php'; ?>

    <?php include 'modules/footer.php'; ?>
    <script src="js/js.js"></script>
</body>

</html>