<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
if (empty($_SESSION) || !isset($_SESSION['yourAccounts'])){
    header('Location: index.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Profil";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Account">
        <div class="side"></div>
        <div id="content">

            <div>
                <div>
                    <?php
                    if(!isset($_SESSION['yourAccounts']->image)){   // FONCTIONNALITÉ FUTUR (COMING SOON...)
                        echo '<img src="/img/users/userDefault.png" alt="votre image de profil">';
                    }
                    else{
                        echo '<img src="'.$_SESSION['yourAccounts']->image.'" alt="votre image de profil">';
                    }
                    ?>
                </div>
                <section>
                    <ul>
                        <li><strong>ID : </strong><?= $_SESSION['yourAccounts']->getID() ?></li>
                        <li><strong>Email : </strong><?= $_SESSION['yourAccounts']->getEmail() ?></li>
                        <li><strong>Prénom : </strong><?= $_SESSION['yourAccounts']->getName() ?></li>
                        <li><strong>Nom : </strong><?= $_SESSION['yourAccounts']->getLastName() ?></li>
                        <li><strong>Ville : </strong><?= $_SESSION['yourAccounts']->getTown() ?></li>
                        <li><strong>Code Postal : </strong><?= $_SESSION['yourAccounts']->getZip() ?></li>
                        <li><strong>Adresse : </strong><?= $_SESSION['yourAccounts']->getAddress() ?></li>
                    </ul>
                </section>
            </div>

        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="/js/js.js"></script>

</body>

</html>