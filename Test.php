<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/adminOnly.php");
?>

<!--JE LAISSE CE FICHIER POUR MONTRER QUE J'AI BIEN EFFECTUÉ LES PREMIERS EXERCICES DU TP3-->

<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Test";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="Connexion">
        <div class="side"></div>
        <div id="content">
            <?php
            echo $bool;

            $sql = 'SELECT email, name, town, updated FROM users';
            echo "<br><br>";
            //connexion à la base de données
            /*
                foreach ($pdo->query($sql) as $row) {
                    echo $row['email'] . "<br>";
                    echo $row['name'] . "<br>";
                    echo $row['town'] . "<br>";
                    echo $row['updated'] . "<br><br>";
                }*/


            //update SQL
            /*
                try {
                    $pdo->beginTransaction();
                    $current_date = date("Y-m-d H:m:s");
                    $pdo->exec("UPDATE users SET updated = '$current_date'");

                    $pdo->commit(); // Les Changements seront enregistrés dans la BDD
                    echo "modifié";
                    
                } catch (\Throwable $th) {
                    $pdo->rollBack(); // Annule les opérations précédentes
                    echo "non modifié";
                }*/


            //Encrypt Passwords
            /**
             * A cause du cryptage des mots de passe il faut lancer la page plusieurs fois avant de compléter les 1500 mot de passe
             */
            /*
                $sql = "SELECT password FROM users WHERE password NOT LIKE '$2y%';";
                foreach ($pdo->query($sql) as $row) {
                    try {
                    
                        $pdo->beginTransaction();
                        
                        $pdo->exec("UPDATE users SET password = '".password_hash($row['password'],PASSWORD_DEFAULT)."' WHERE password = '".$row['password']."'");
                        $pdo->commit(); // Les Changements seront enregistrés dans la BDD
                        echo "modifié <br>";
                        
                    } catch (Exception $e) {
                        echo $e->getLine()." : ".$e->getMessage();
                        $pdo->rollBack(); // Annule les opérations précédentes
                        echo "non modifié <br>";
                    }
                }*/



            ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="/js/js.js"></script>

</body>

</html>