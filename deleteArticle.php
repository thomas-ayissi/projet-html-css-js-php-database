<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/preRender.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/modules/adminOnly.php");

if(isset($_GET['delete'])&& !empty($_GET['delete'])){
    try {
        $pdo->beginTransaction();

        $pdo->exec('DELETE FROM "articles" 
                    WHERE "id" = '.$pdo->quote($_GET['delete']));

        $row = $pdo->query('SELECT * FROM "articles" WHERE "id" > '.$pdo->quote($_GET['delete']))->fetchAll();
        if(!empty($row)){
            foreach ($row as $article) {
                $pdo->exec('UPDATE "articles"
                            SET "id" = "'.($article['id']-1).'" WHERE "id" = "'.$article['id'].'";');
            }
        }

        $pdo->commit();
    } catch (\Throwable $th) {
        $pdo->rollBack();
    }

    header('Location: blog.php');
    exit();
}

function getArticle($pdo){
    if(isset($_GET["search"])&&!empty($_GET["search"])){
        $lowerSearch = strtolower($_GET['search']);
        $sql = "SELECT * FROM articles WHERE lower(title) LIKE '%" . $lowerSearch . "%' ";
    }
    else{
        $sql = "SELECT * FROM articles";
    }
    
    $articlesHTML = "";

    try {
        $row = $pdo->query($sql)->fetchAll();
        $row = array_reverse($row);
        foreach ($row as $article){
            $articlesHTML .= '
                <div class="arti">
                    <img src="'.$article['image'].'" alt="'.$article['title'].'">
                    <p>'.$article['title'].'</p>
                    <div>
                        <a href="/deleteArticle.php?delete='.$article['id'].'">&#10006;</a>
                    </div>
                </div>
            ';
        }
        return $articlesHTML;
    } catch (\Throwable $th) {
        return NULL;
    }
    
}

?>
<!DOCTYPE html>
<html lang="fr">
<?php
$name = "Supprimer un Article";
include 'modules/head.php';
?>

<body>
    <?php include 'modules/header.php'; ?>

    <main id="deleteArticle">
        <div class="side"></div>
        <div id="content">
            <form class="searchBar" action="/deleteArticle.php" method="get">
                <input type="search" placeholder=" Rechercher un article..." name="search">
                <button type="submit"><span id="magnifying_glass">&#128269;</span></button>
            </form>
            <?php
                echo getArticle($pdo);

            ?>
        </div>
        <div class="side"></div>
    </main>

    <?php include 'modules/footer.php'; ?>

    <script src="/js/js.js"></script>

</body>

</html>